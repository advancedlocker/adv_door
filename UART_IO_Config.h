/**
* \addtogroup MOD_UART
* \{
**/

/**
* \file
* \brief Configuration include file for \ref MOD_UART
* \author Alex Mykyta
**/

#ifndef __UART_IO_CONFIG_H__
#define __UART_IO_CONFIG_H__

//==================================================================================================
/// \name Configuration
/// Configuration defines for the \ref MOD_UART module
/// \{
//==================================================================================================

//  ===================================================
//  = NOTE: Actual ports must be configured manually! =
//  ===================================================
#define UART_TXD   0x10                     // TXD on P3.4
#define UART_RXD   0x20                     // RXD on P3.5

//--------------------------------------------------------------------------------------------------
// RX Settings
//--------------------------------------------------------------------------------------------------

#define UIO_RX_MODE         1
/**<    0 = Polling Mode    : No buffers used. RX is done directly by polling within function calls. \n
*       1 = Interrupt Mode  : RX triggers an interrupt and data is pushed into a FIFO. \n
*       2 = DMA Mode        : RX data is automatically loaded into a buffer using DMA.
**/

// RX buffer size (modes 1 & 2 only)
#define UIO_RXBUF_SIZE      128    ///< \hideinitializer

// RX DMA Channel (mode 2 only)
#define UIO_RX_DMA_CHANNEL  0

// RX DMA Trigger Source
//  Choose the corresponding trigger (see datasheet) that matches the DMA channel and UART RX device
#define RX_DMA_TSEL         // [LOOKUP IN DATASHEET!]

//--------------------------------------------------------------------------------------------------
// TX Settings
//--------------------------------------------------------------------------------------------------

#define UIO_TX_MODE         1
/**<    0 = Polling Mode    : No buffers used. TX functions will block until data is transmitted. \n
*       1 = Interrupt Mode  : TX data is buffered. TX operations are handled using interrupts. \n
**/

// TX buffer size (modes 1 & 2 only)
#define UIO_TXBUF_SIZE      128    ///< \hideinitializer

//--------------------------------------------------------------------------------------------------
// Common Settings
//--------------------------------------------------------------------------------------------------

/// Select which USCI/USART module to use
#define UIO_USE_DEV         0    ///< \hideinitializer
/**<    0 = USCIA0 \n
*       1 = USCIA1 \n
*       2 = USCIA2 \n
*       3 = USCIA3
**/

/// Select which clock source to use
#define UIO_CLK_SRC         2    ///< \hideinitializer
/**<    0 = External \n
*       1 = ACLK     \n
*       2 = SMCLK
**/


// Modulation register settings for MSP430 are annoying to automatically determine at compile time.
// Instead, fill them out here.
// Note: MCTL implementations vary based on UART architectures. Most online calculators will only
//   generate MCTL values for the 1xx version.

// UART clock = [...]
// Baud = [...]

// Baud rate register settings for USART (found in 1xx and 4xx devices)
//#define USART_BR0_VALUE     0x00
//#define USART_BR1_VALUE     0x00
//#define USART_MCTL_VALUE    0x00

// Baud rate register settings for USCI (found in 2xx, 4xx, 5xx, and 6xx devices)
///38400 \ref http://www.ti.com/lit/ug/slau144j/slau144j.pdf \page 425 & \ref http://processors.wiki.ti.com/index.php/USCI_UART_Baud_Rate_Gen_Mode_Selection?keyMatch=baud%20rate%20calculator&tisearch=Search-EN
//#define USCI_BR0_VALUE      0xA0              //N = clk/baudrate = 416 for 38400
//#define USCI_BR1_VALUE      0x01
//#define USCI_MCTL_VALUE     0x0C              //UCBRF=0, UCBRS=6, UCSOS16 = 0

//#define USCI_INPUT_CLK              (1048576UL)     // in Hz
#define USCI_INPUT_CLK              (7372800UL)     // in Hz
#define USCI_BAUD_RATE              (38400)        // MAX 1M buad

#define USCI_DIV_INT                ((USCI_INPUT_CLK/USCI_BAUD_RATE))
#define USCI_DIV_FRAC_NUMERATOR     (USCI_INPUT_CLK - (USCI_DIV_INT*USCI_BAUD_RATE))
#define USCI_DIV_FRAC_NUM_X_8       (USCI_DIV_FRAC_NUMERATOR*8)
#define USCI_DIV_FRAC_X_8           (USCI_DIV_FRAC_NUM_X_8/USCI_BAUD_RATE)

#if (((USCI_DIV_FRAC_NUM_X_8-(USCI_DIV_FRAC_X_8*USCI_BAUD_RATE))*10)/USCI_BAUD_RATE < 5)
#define USCI_BRS_VAL                (USCI_DIV_FRAC_X_8 << 1)
#else
#define USCI_BRS_VAL                ((USCI_DIV_FRAC_X_8+1)<< 1)
#endif

#define UCBRS                       USCI_BRS_VAL
#define UCBRF                       0

#define USART_BR0_VALUE             (USCI_DIV_INT & 0x00FF)
#define USART_BR1_VALUE             ((USCI_DIV_INT >> 8) & 0xFF)
#define USART_MCTL_VALUE            ((((UCBRF  & 0xF) << 4) | ((UCBRS & 0x7) << 1)))

// Baud rate register settings for eUSCI (found in 6xx devices)
#define EUSCI_BR0_VALUE     0x00
#define EUSCI_BR1_VALUE     0x00
#define EUSCI_MCTL_VALUE    0x0000

///\}

#endif
///\}
