#ifndef _EEPROM_H_
#define _EEPROM_H_

void EEPROM_init(void);
uint8_t EEPROM_read(char *data, uint16_t data_length);
uint8_t EEPROM_write(char *data, uint16_t data_length);
uint8_t EEPROM_Get_Serial(uint32_t *Serial);

#endif //  _EEPROM_H_

