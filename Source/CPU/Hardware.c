/*
 * Hardware.c
 *
 *  Created on: Mar 19, 2018
 *      Author: Patrick
 */

///INCLUDES
#include "Hardware.h"
#include "GPIO.h"
#include "Clock.h"
#include "Timers.h"
#include "UART_IO.h"

///DEFINES

///FUNCTIONS
void hardware_init(void)
{
#if __MSP430_HEADER_VERSION__
    {
        clock_set(CLOCK_SOURCE_EXTERNAL_HF_OSCILLATOR,CLOCK_FREQUENCY_CRYSTAL_3__16MHz);        ///init MCU clock source
        //clock_set(CLOCK_SOURCE_DIGITAL_CONTROLLED_OSCILLATOR,CLOCK_FREQUENCY_DCO_8MHz);       ///init MCU clock source
        //Set_DCO();
        gpio_init();                                                                            ///init gpio's
        start_timer();
        uart_init();
        //__enable_interrupt();
        _BIS_SR(GIE);
    }
#endif
}

void hardware_deinit(void)
{
#if __MSP430_HEADER_VERSION__
    {
        gpio_deinit();                                                                          ///init gpio's
    }
#endif
}




