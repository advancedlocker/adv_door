/*
 * Timers.h
 *
 *  Created on: Mar 19, 2018
 *      Author: Patrick
 */

#ifndef SOURCE_TIMERS_H_
#define SOURCE_TIMERS_H_

#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

void start_timer(void);

#ifdef __cplusplus
}
#endif

#endif /* SOURCE_TIMERS_H_ */
