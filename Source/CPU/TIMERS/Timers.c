/*
 * Timers.c
 *
 *  Created on: Mar 19, 2018
 *      Author: Patrick
 */
#include <msp430.h>
#include "Vlocker_Config.h"
#include "lib_uptime.h"

#define ENABLE_WHILE    0

void start_timer(void)
{
    CCTL0   =   CCIE;
    CCR0    =   7373;
    TACTL   =   TASSEL_2 + MC_1; // Set the timer A to SMCLK, count Up to CCR0 */
    // Clear the timer and enable timer interrupt
}

// Timer A0 interrupt service routine
#if defined(__TI_COMPILER_VERSION__) || defined(__IAR_SYSTEMS_ICC__)
#pragma vector=TIMER0_A0_VECTOR
__interrupt void Timer_A (void)
#elif defined(__GNUC__)
void __attribute__ ((interrupt(TIMER0_A0_VECTOR))) Timer_A (void)
#else
#error Compiler not supported!
#endif
{
    uptime_ms_++;
}

#pragma vector=WDT_VECTOR
__interrupt void WDT_isr (void)
{
    //do nothing
    while (ENABLE_WHILE)
    {

    }
}

#pragma vector=COMPARATORA_VECTOR
__interrupt void COMPARATORA_VECTOR_isr (void)
{
    //do nothing
    while (ENABLE_WHILE)
    {

    }
}

#pragma vector=NMI_VECTOR
__interrupt void NMI_VECTOR_isr (void)
{
    //do nothing
    while (ENABLE_WHILE)
    {

    }
}

#pragma vector=RESET_VECTOR
__interrupt void RESET_VECTOR_isr (void)
{
    //do nothing
    while (ENABLE_WHILE)
    {

    }
}

#pragma vector=TRAPINT_VECTOR
__interrupt void TRAPINT_VECTOR_isr (void)
{
    //do nothing
    while (ENABLE_WHILE)
    {

    }
}

