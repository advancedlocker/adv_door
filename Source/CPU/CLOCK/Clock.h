/*
 * Clock.h
 *
 *  Created on: Mar 19, 2018
 *      Author: Patrick
 */

#ifndef SOURCE_CLOCK_H_
#define SOURCE_CLOCK_H_


#include "Result.h"

#ifdef __cplusplus
extern "C" {
#endif


//DEFINES
typedef enum e_clock_source
{
    CLOCK_SOURCE_DIGITAL_CONTROLLED_OSCILLATOR,
    CLOCK_SOURCE_EXTERNAL_HF_OSCILLATOR,
    CLOCK_SOURCE_EXTERNAL_LF_OSCILLATOR,
    CLOCK_SOURCE_INTERNAL_LF_OSCILLATOR,
}e_clock_source_t;

typedef enum e_clock_frequency
{
    CLOCK_FREQUENCY_CRYSTAL_0_4__1MHz,
    CLOCK_FREQUENCY_CRYSTAL_1__3MHz,
    CLOCK_FREQUENCY_CRYSTAL_3__16MHz,
    CLOCK_FREQUENCY_DIGITAL_0_4__16_MHz,
    CLOCK_FREQUENCY_DCO_1MHZ,
    CLOCK_FREQUENCY_DCO_8MHz,
    CLOCK_FREQUENCY_DCO_12MHz,
    CLOCK_FREQUENCY_DCO_16MHz,
}e_clock_frequency_t;

void Set_DCO (void);

/**
 * @brief set mcu clock
 * @param type of source clock
 * @param frequency of the source clock
 * @return result
 */
RES_t clock_set(e_clock_source_t type, e_clock_frequency_t frequency);

/**
 * @brief gets the current clock source
 * @param clock_source
 * @param frequency
 * @return result
 */
RES_t clock_get(e_clock_source_t *clock_source, e_clock_frequency_t *frequency);

#ifdef __cplusplus
}
#endif

#endif /* SOURCE_CLOCK_H_ */
