/*
 * Clock.c
 *
 *  Created on: Mar 19, 2018
 *      Author: Patrick
 */

///INCLUDES
#include "Clock.h"
#include "Vlocker_Config.h"

///DEFINES

typedef struct s_debug_info
{
    uint16_t        memory_used;
}s_clock_debug_t;


typedef struct s_clock_config_t
{

}s_clock_config_t;

typedef struct s_clock_info
{
    e_clock_source_t    clock;
    e_clock_frequency_t frequency;
}s_clock_info_t;

typedef struct s_clock_device
{
    s_clock_info_t      info;
    s_clock_config_t    config;
    s_clock_debug_t     debug;
}s_clock_device_t;

static s_clock_device_t s_device_clock;

///FUNCTIONS

RES_t clock_set(e_clock_source_t type, e_clock_frequency_t frequency)
{
    ///setup system clocks see section 5.2.7.1 of slau144j.pdf page 280.
    s_device_clock.info.clock = type;
    s_device_clock.info.frequency = frequency;

    switch(type)
    {
        case CLOCK_SOURCE_DIGITAL_CONTROLLED_OSCILLATOR:
        {
            BCSCTL1 = RSEL2+RSEL1+RSEL0; //Set max DCO frequency
            DCOCTL = DCO2+DCO1+DCO0;
        }
        break;
        case CLOCK_SOURCE_EXTERNAL_HF_OSCILLATOR:
        {
            //BCSCTL1 = (XT2OFF|XTS|DIVA_0);          //XT2 is off, High frequency mode, /2 divider for ACLK
            BCSCTL1 = DIVA_0;          //Low frequency mode, /1 divider for ACLK
            BCSCTL1 &= ~XT2OFF;        //XT2 is on
            //loop till OFIFG flag is cleared
            while(IFG1 & OFIFG)
            {
                //clear the OFIFIG flag
                 IFG1 &= ~OFIFG;

                 _delay_cycles(1000);
            }
            BCSCTL2 = (SELM_2|DIVM_0|SELS|DIVS_0);  //MCLK sourced from LFXT1CLK, /2 divider for MCLK, SMCLK sourced from LFXT1CLK, /2 divider for SMCLK
            //_bis_SR_register(SCG0);                 // turnoff DCO

        }
        break;
        case CLOCK_SOURCE_EXTERNAL_LF_OSCILLATOR:
        {

        }
        break;
        case CLOCK_SOURCE_INTERNAL_LF_OSCILLATOR:
        {

        }
        break;
        default:
        break;
    }
    return RES_OK;
}


RES_t clock_get(e_clock_source_t *clock_source, e_clock_frequency_t *frequency)
{
    *clock_source = s_device_clock.info.clock;
    *frequency = s_device_clock.info.frequency;
    return RES_OK;
}

//------------------------------------------------------------------------------
void Set_DCO (void)                         // Set DCO to selected frequency
//------------------------------------------------------------------------------
{
#define DELTA 256                           // Target DCO = DELTA*(4096) = 1048576

  unsigned int Compare, Oldcapture = 0;

  BCSCTL1 |= DIVA_3;                        // ACLK= LFXT1CLK/8

  CCTL2 = CM_1 + CCIS_1 + CAP;              // CAP, ACLK
  TACTL = TASSEL_2 + MC_2 + TACLR;          // SMCLK, cont-mode, clear

  while (1)
  {
    while (!(CCIFG & CCTL2));               // Wait until capture occured
    CCTL2 &= ~CCIFG;                        // Capture occured, clear flag
    Compare = CCR2;                         // Get current captured SMCLK
    Compare = Compare - Oldcapture;         // SMCLK difference
    Oldcapture = CCR2;                      // Save current captured SMCLK
    if (DELTA == Compare) break;            // If equal, leave "while(1)"

    else if (DELTA < Compare)
    {
      DCOCTL--;
      if (DCOCTL == 0xFF)                   // DCO is too fast, slow it down
      {
        if (!(BCSCTL1 == (XT2OFF + DIVA_3)))
        BCSCTL1--;                          // Did DCO role under?, Sel lower RSEL
      }
    }
    else
    {
      DCOCTL++;                             // DCO is too slow, speed it down
      if (DCOCTL == 0x00)
      {
        if (!(BCSCTL1 == (XT2OFF + DIVA_3 + 0x07)))
        BCSCTL1++;                          // Did DCO role over? Sel higher RSEL
      }
    }
  }

    while(IFG1 & OFIFG)
    {
        //clear the OFIFIG flag
        IFG1 &= ~OFIFG;

        _delay_cycles(1000);
    }

  CCTL2 = 0;                                // Stop CCR2
  TACTL = 0;                                // Stop Timer_A
}

