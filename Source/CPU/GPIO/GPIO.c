/*
 * GPIO.c
 *
 *  Created on: Mar 19, 2018
 *      Author: Patrick
 */
//INCLUDES
#include "gpio.h"
#include "result.h"

//DEFINES
typedef struct s_device_mcu_gpio_info
{

}s_device_mcu_gpio_info_t;


typedef struct
{
        void        (*callback_port1_pin0)(transition_t transition);
        void        (*callback_port1_pin1)(transition_t transition);
        void        (*callback_port1_pin2)(transition_t transition);
        void        (*callback_port1_pin3)(transition_t transition);
        void        (*callback_port1_pin4)(transition_t transition);
        void        (*callback_port1_pin5)(transition_t transition);
        void        (*callback_port1_pin6)(transition_t transition);
        void        (*callback_port1_pin7)(transition_t transition);
        void        (*callback_port2_pin0)(transition_t transition);
        void        (*callback_port2_pin1)(transition_t transition);
        void        (*callback_port2_pin2)(transition_t transition);
        void        (*callback_port2_pin3)(transition_t transition);
        void        (*callback_port2_pin4)(transition_t transition);
        void        (*callback_port2_pin5)(transition_t transition);
        void        (*callback_port2_pin6)(transition_t transition);
        void        (*callback_port2_pin7)(transition_t transition);
}s_device_mcu_gpio_callbacks_t;

typedef struct
{
    s_device_mcu_gpio_callbacks_t       callbacks;

}s_device_mcu_gpio_config_t;

typedef struct
{
    uint8_t     memory_used;
}s_device_mcu_gpio_debug_t;

typedef struct
{
    s_device_mcu_gpio_info_t        info;
    s_device_mcu_gpio_config_t      config;
    s_device_mcu_gpio_debug_t       debug;
}s_device_mcu_gpio_t;


s_device_mcu_gpio_t s_device_mcu_gpio;


//FUNCTIONS
void gpio_init()
{
    memset(&s_device_mcu_gpio,0x0000,sizeof(s_device_mcu_gpio_t));
    s_device_mcu_gpio.debug.memory_used = sizeof(s_device_mcu_gpio_t);

    ///todo set unused pins according to section 8.2.8 of slau144j.pdf
    /*
     "Unused I/O pins should be configured as I/O function, output direction, and left unconnected on the PCB
        board, to prevent a floating input and reduce power consumption. The value of the PxOUT bit is irrelevant,
        since the pin is unconnected. Alternatively, the integrated pullup/pulldown resistor can be enabled by
        setting the PxREN bit of the unused pin to prevent the floating input. See the System Resets, Interrupts,
        and Operating Modes chapter for termination of unused pins."
     */
}


void gpio_set_function(uint8_t port, uint8_t pin, e_PnSEL_t select, e_PnSEL_ENABLE_DISABLE_t enable_disable)
{
    switch(select)
    {
        case PnSEL:
        {
            switch(enable_disable)
            {
                case ENABLE:
                {
                    switch(port)
                    {
                        case PORT1:
                        {
                            P1SEL |= (1 << pin);
                        }
                        break;

                        case PORT2:
                        {
                            P2SEL |= (1 << pin);
                        }
                        break;

                        case PORT3:
                        {
                            P3SEL |= (1 << pin);
                        }
                        break;

                        case PORT4:
                        {
                            P4SEL |= (1 << pin);
                        }
                        break;

                        default:
                        break;
                    }
                }
                break;

                case DISABLE:
                {
                    switch(port)
                    {
                        case PORT1:
                        {
                            P1SEL &= ~(1 << pin);
                        }
                        break;

                        case PORT2:
                        {
                            P2SEL &= ~(1 << pin);
                        }
                        break;

                        case PORT3:
                        {
                            P3SEL &= ~(1 << pin);
                        }
                        break;

                        case PORT4:
                        {
                            P4SEL &= ~(1 << pin);
                        }
                        break;

                        default:
                        break;
                    }
                }
                break;

                default:
                break;
            }
        }
        break;
        default:
        break;
    }
}

void gpio_set_direction(uint8_t port, uint8_t pin, e_IO_DIRECTION_t direction)
{
    switch(direction)
    {
        case INTERNAL:
            switch(port)
            {
                case PORT1:
                {
                    P1DIR &= ~(1 << pin);
                }
                break;

                case PORT2:
                {
                    P2DIR &= ~(1 << pin);
                }
                break;

                case PORT3:
                {
                    P3DIR &= ~(1 << pin);
                }
                break;

                case PORT4:
                {
                    P4DIR &= ~(1 << pin);
                }
                break;

                default:
                break;
            }
        break;

        case EXTERNAL:
        {
            switch(port)
            {
                case PORT1:
                {
                    P1DIR |= (1 << pin);
                }
                break;

                case PORT2:
                {
                    P2DIR |= (1 << pin);
                }
                break;

                case PORT3:
                {
                    P3DIR |= (1 << pin);
                }
                break;

                case PORT4:
                {
                    P4DIR |= (1 << pin);
                }
                break;

                default:
                break;
            }
        }
        break;

        default:
        break;
    }
}

void gpio_set_pull_resistor(uint8_t port, uint8_t pin, bool enable)
{
    switch(enable)
    {
        case false:
            switch(port)
            {
                case PORT1:
                {

                }
                break;
                case PORT2:
                {

                }
                break;
                case PORT3:
                {

                }
                break;
                case PORT4:
                {

                }
                break;
                default:
                break;
            }
        break;

        case true:
        {
            switch(port)
            {
                case PORT1:
                {

                }
                break;
                case PORT2:
                {

                }
                break;
                case PORT3:
                {

                }
                break;
                case PORT4:
                {

                }
                break;
                default:
                break;
            }
        }
        break;

        default:
        break;
    }
}

e_IO_LEVEL_t gpio_get(uint8_t port, uint8_t pin)
{

    uint8_t input = 0;

    switch(port)
    {
        case PORT1:
        {
            input = P1IN;
        }
        break;

        case PORT2:
        {
            input = P2IN;
        }
        break;

        case PORT3:
        {
            input = P3IN;
        }
        break;

        case PORT4:
        {
            input = P4IN;
        }
        break;

        default:
        break;
    }

    input = input >> pin;
    input &= BIT0;

    if(input)
    {
        return(HIGH);
    }
    else
    {
        return(LOW);
    }
}

e_IO_LEVEL_t gpio_get_output(uint8_t port, uint8_t pin)
{

    uint8_t input = 0;

    switch(port)
    {
        case PORT1:
        {
            input = P1OUT;
        }
        break;

        case PORT2:
        {
            input = P2OUT;
        }
        break;

        case PORT3:
        {
            input = P3OUT;
        }
        break;

        case PORT4:
        {
            input = P4OUT;
        }
        break;

        default:
        break;
    }

    //if(input & (1 << pin))
    if(input >> pin == HIGH)
    {
        return(HIGH);
    }
    else
    {
        return(LOW);
    }
}

void gpio_set(uint8_t port, uint8_t pin, e_IO_LEVEL_t level)
{
    ///read in value of port and then set pin accordingly
    uint8_t output = 0;

    switch(level)
    {
        case HIGH:

            switch(port)
            {
                case PORT1:
                {
                    output = P1IN;
                    output |= (1 << pin);

                    P1OUT = output;
                }
                break;

                case PORT2:
                {
                    output = P2IN;
                    output |= (1 << pin);

                    P2OUT = output;
                }
                break;

                case PORT3:
                {
                    output = P3IN;
                    output |= (1 << pin);

                    P3OUT = output;

                }
                break;

                case PORT4:
                {
                    output = P4IN;
                    output |= (1 << pin);

                    P4OUT = output;
                }
                break;

                default:
                break;
            }
        break;

        case LOW:
        {
            switch(port)
            {
                case PORT1:
                {
                    output = P1IN;
                    output &= ~(1 << pin);

                    P1OUT = output;
                }
                break;

                case PORT2:
                {
                    output = P2IN;
                    output &= ~(1 << pin);

                    P2OUT = output;
                }
                break;

                case PORT3:
                {
                    output = P3IN;
                    output &= ~(1 << pin);

                    P3OUT = output;
                }
                break;

                case PORT4:
                {
                    output = P4IN;
                    output &= ~(1 << pin);

                    P4OUT = output;
                }
                break;

                default:
                break;
            }
        }
        break;

        default:
        break;
    }
}

void gpio_set_edge_interrupt(uint8_t port, uint8_t pin,transition_t transition, void (*callback)(transition_t transition), bool enable)
{
    switch(port)
    {
        case PORT1:
        {
            ///enable interrupt edge type
            if(transition)
            {
                P1IES |= (1 << pin);            //setting bit high in P1IES for pin enables an interrupt with a high to low transition.
            }
            else
            {
                P1IES &= ~(1 <<pin);            //setting bit low in P1IES for pin enables an interrupt with a low to high transition.
            }

            ///set callback
            switch(pin)
            {
                case PIN0:
                {
                    s_device_mcu_gpio.config.callbacks.callback_port1_pin0 = callback;
                }
                break;

                case PIN1:
                {
                    s_device_mcu_gpio.config.callbacks.callback_port1_pin1 = callback;
                }
                break;

                case PIN2:
                {
                    s_device_mcu_gpio.config.callbacks.callback_port1_pin2 = callback;
                }
                break;

                case PIN3:
                {
                    s_device_mcu_gpio.config.callbacks.callback_port1_pin3 = callback;
                }
                break;

                case PIN4:
                {
                    s_device_mcu_gpio.config.callbacks.callback_port1_pin4 = callback;
                }
                break;

                case PIN5:
                {
                    s_device_mcu_gpio.config.callbacks.callback_port1_pin5 = callback;
                }
                break;

                case PIN6:
                {
                    s_device_mcu_gpio.config.callbacks.callback_port1_pin6 = callback;
                }
                break;

                case PIN7:
                {
                    s_device_mcu_gpio.config.callbacks.callback_port1_pin7 = callback;
                }
                break;

                default:
                break;
            }

            ///enable interrupt
            if(enable)
            {
                P1IE |= (1 << pin);             //enable Port 1 pin n interrupt, note: this interrupt is disabled when PxSELx is set
            }
            else
            {
                P1IE &= ~(1 <<pin);             //disable Port 1 pin n interrupt, note: this interrupt is disabled when PxSELx is set
            }
        }
        break;

        case PORT2:
        {
            ///enable interrupt edge type
            if(transition)
            {
                P2IES |= (1 << pin);            //setting bit high in P2IES for pin enables an interrupt with a high to low transition.
            }
            else
            {
                P2IES &= ~(1 <<pin);            //setting bit low in P2IES for pin enables an interrupt with a low to high transition.
            }

            ///set callback
            switch(pin)
            {
                case PIN0:
                {
                    s_device_mcu_gpio.config.callbacks.callback_port2_pin0 = callback;
                }
                break;

                case PIN1:
                {
                    s_device_mcu_gpio.config.callbacks.callback_port2_pin1 = callback;
                }
                break;

                case PIN2:
                {
                    s_device_mcu_gpio.config.callbacks.callback_port2_pin2 = callback;
                }
                break;

                case PIN3:
                {
                    s_device_mcu_gpio.config.callbacks.callback_port2_pin3 = callback;
                }
                break;

                case PIN4:
                {
                    s_device_mcu_gpio.config.callbacks.callback_port2_pin4 = callback;
                }
                break;

                case PIN5:
                {
                    s_device_mcu_gpio.config.callbacks.callback_port2_pin5 = callback;
                }
                break;

                case PIN6:
                {
                    s_device_mcu_gpio.config.callbacks.callback_port2_pin6 = callback;
                }
                break;

                case PIN7:
                {
                    s_device_mcu_gpio.config.callbacks.callback_port2_pin7 = callback;
                }
                break;

                default:
                break;
            }

            ///enable interrupt
            if(enable)
            {
                P2IE |= (1 << pin);             //enable Port 1 pin n interrupt, note: this interrupt is disabled when PxSELx is set
            }
            else
            {
                P2IE &= ~(1 <<pin);             //disable Port 1 pin n interrupt, note: this interrupt is disabled when PxSELx is set
            }
        }
        break;

        default:
        break;
    }
}


#pragma vector=PORT1_VECTOR
__interrupt void Port_1(void)
{
    uint8_t pin = P1IFG;
    uint8_t transition = P1IES;



    ///scan through P1IFG
    uint8_t i;

    for(i=0;i<8;i++)
    {
        if(((pin >> i) & 1)  == true)
        {
            switch(i)
            {
                case 0:
                {
                    P1IFG &= ~BIT0;     //clear interrupt flag;

                    if(s_device_mcu_gpio.config.callbacks.callback_port1_pin0 != NULL)
                    {
                        if(transition & BIT0)
                        {
                            s_device_mcu_gpio.config.callbacks.callback_port1_pin0(HIGH_TO_LOW);
                        }
                        else
                        {
                            s_device_mcu_gpio.config.callbacks.callback_port1_pin0(LOW_TO_HIGH);
                        }
                    }
                }
                break;

                case 1:
                {
                    P1IFG &= ~BIT1;     //clear interrupt flag;

                    if(s_device_mcu_gpio.config.callbacks.callback_port1_pin1 != NULL)
                    {
                        if(transition & BIT1)
                        {
                            s_device_mcu_gpio.config.callbacks.callback_port1_pin1(HIGH_TO_LOW);
                        }
                        else
                        {
                            s_device_mcu_gpio.config.callbacks.callback_port1_pin1(LOW_TO_HIGH);
                        }
                    }
                }
                break;

                case 2:
                {
                    P1IFG &= ~BIT2;     //clear interrupt flag;

                    if(s_device_mcu_gpio.config.callbacks.callback_port1_pin2 != NULL)
                    {
                        if(transition & BIT2)
                        {
                            s_device_mcu_gpio.config.callbacks.callback_port1_pin2(HIGH_TO_LOW);
                        }
                        else
                        {
                            s_device_mcu_gpio.config.callbacks.callback_port1_pin2(LOW_TO_HIGH);
                        }
                    }
                }
                break;

                case 3:
                {
                    P1IFG &= ~BIT3;     //clear interrupt flag;

                    if(s_device_mcu_gpio.config.callbacks.callback_port1_pin3 != NULL)
                    {
                        if(transition & BIT3)
                        {
                            s_device_mcu_gpio.config.callbacks.callback_port1_pin3(HIGH_TO_LOW);
                        }
                        else
                        {
                            s_device_mcu_gpio.config.callbacks.callback_port1_pin3(LOW_TO_HIGH);
                        }
                    }
                }
                break;

                case 4:
                {
                    P1IFG &= ~BIT4;     //clear interrupt flag;

                    if(s_device_mcu_gpio.config.callbacks.callback_port1_pin4 != NULL)
                    {
                        if(transition & BIT4)
                        {
                            s_device_mcu_gpio.config.callbacks.callback_port1_pin4(HIGH_TO_LOW);
                        }
                        else
                        {
                            s_device_mcu_gpio.config.callbacks.callback_port1_pin4(LOW_TO_HIGH);
                        }
                    }
                }
                break;

                case 5:
                {
                    P1IFG &= ~BIT5;     //clear interrupt flag;

                    if(s_device_mcu_gpio.config.callbacks.callback_port1_pin5 != NULL)
                    {
                        if(transition & BIT5)
                        {
                            s_device_mcu_gpio.config.callbacks.callback_port1_pin5(HIGH_TO_LOW);
                        }
                        else
                        {
                            s_device_mcu_gpio.config.callbacks.callback_port1_pin5(LOW_TO_HIGH);
                        }
                    }
                }
                break;

                case 6:
                {
                    P1IFG &= ~BIT6;     //clear interrupt flag;

                    if(s_device_mcu_gpio.config.callbacks.callback_port1_pin6 != NULL)
                    {
                        if(transition & BIT6)
                        {
                            s_device_mcu_gpio.config.callbacks.callback_port1_pin6(HIGH_TO_LOW);
                        }
                        else
                        {
                            s_device_mcu_gpio.config.callbacks.callback_port1_pin6(LOW_TO_HIGH);
                        }
                    }
                }
                break;

                case 7:
                {
                    P1IFG &= ~BIT7;     //clear interrupt flag;

                    if(s_device_mcu_gpio.config.callbacks.callback_port1_pin7 != NULL)
                    {
                        if(transition & BIT7)
                        {
                            s_device_mcu_gpio.config.callbacks.callback_port1_pin7(HIGH_TO_LOW);
                        }
                        else
                        {
                            s_device_mcu_gpio.config.callbacks.callback_port1_pin7(LOW_TO_HIGH);
                        }
                    }
                }
                break;
            }
        }
    }
}

#pragma vector=PORT2_VECTOR
__interrupt void Port_2(void)
{
    uint8_t pin = P2IFG;
    uint8_t transition = P2IES;

    ///scan through P2IFG
        uint8_t i;

        for(i=0;i<8;i++)
        {
            if(((pin >> i) & 1)  == true)
            {
                switch(i)
                {
                    case 0:
                    {
                        P2IFG &= ~BIT0;     //clear interrupt flag;

                        if(s_device_mcu_gpio.config.callbacks.callback_port2_pin0 != NULL)
                        {
                            if(transition & BIT0)
                            {
                                s_device_mcu_gpio.config.callbacks.callback_port2_pin0(HIGH_TO_LOW);
                            }
                            else
                            {
                                s_device_mcu_gpio.config.callbacks.callback_port2_pin0(LOW_TO_HIGH);
                            }
                        }
                    }
                    break;

                    case 1:
                    {
                        P2IFG &= ~BIT1;     //clear interrupt flag;

                        if(s_device_mcu_gpio.config.callbacks.callback_port2_pin1 != NULL)
                        {
                            if(transition & BIT1)
                            {
                                s_device_mcu_gpio.config.callbacks.callback_port2_pin1(HIGH_TO_LOW);
                            }
                            else
                            {
                                s_device_mcu_gpio.config.callbacks.callback_port2_pin1(LOW_TO_HIGH);
                            }
                        }
                    }
                    break;

                    case 2:
                    {
                        P2IFG &= ~BIT2;     //clear interrupt flag;

                        if(s_device_mcu_gpio.config.callbacks.callback_port2_pin2 != NULL)
                        {
                            if(transition & BIT2)
                            {
                                s_device_mcu_gpio.config.callbacks.callback_port2_pin2(HIGH_TO_LOW);
                            }
                            else
                            {
                                s_device_mcu_gpio.config.callbacks.callback_port2_pin2(LOW_TO_HIGH);
                            }
                        }
                    }
                    break;

                    case 3:
                    {
                        P2IFG &= ~BIT3;     //clear interrupt flag;

                        if(s_device_mcu_gpio.config.callbacks.callback_port2_pin3 != NULL)
                        {
                            if(transition & BIT3)
                            {
                                s_device_mcu_gpio.config.callbacks.callback_port2_pin3(HIGH_TO_LOW);
                            }
                            else
                            {
                                s_device_mcu_gpio.config.callbacks.callback_port2_pin3(LOW_TO_HIGH);
                            }
                        }
                    }
                    break;

                    case 4:
                    {
                        P2IFG &= ~BIT4;     //clear interrupt flag;

                        if(s_device_mcu_gpio.config.callbacks.callback_port2_pin4 != NULL)
                        {
                            if(transition & BIT4)
                            {
                                s_device_mcu_gpio.config.callbacks.callback_port2_pin4(HIGH_TO_LOW);
                            }
                            else
                            {
                                s_device_mcu_gpio.config.callbacks.callback_port2_pin4(LOW_TO_HIGH);
                            }
                        }
                    }
                    break;

                    case 5:
                    {
                        P2IFG &= ~BIT5;     //clear interrupt flag;

                        if(s_device_mcu_gpio.config.callbacks.callback_port2_pin5 != NULL)
                        {
                            if(transition & BIT5)
                            {
                                s_device_mcu_gpio.config.callbacks.callback_port2_pin5(HIGH_TO_LOW);
                            }
                            else
                            {
                                s_device_mcu_gpio.config.callbacks.callback_port2_pin5(LOW_TO_HIGH);
                            }
                        }

                        ///special case were the device has to wake up from hibernation when power is applied to power connector, and to exit LPM4 it has to be done within an interrupt.
                        LPM4_EXIT;
                    }
                    break;

                    case 6:
                    {
                        P2IFG &= ~BIT6;     //clear interrupt flag;

                        if(s_device_mcu_gpio.config.callbacks.callback_port2_pin6 != NULL)
                        {
                            if(transition & BIT6)
                            {
                                s_device_mcu_gpio.config.callbacks.callback_port2_pin6(HIGH_TO_LOW);
                            }
                            else
                            {
                                s_device_mcu_gpio.config.callbacks.callback_port2_pin6(LOW_TO_HIGH);
                            }
                        }
                    }
                    break;

                    case 7:
                    {
                        P2IFG &= ~BIT7;     //clear interrupt flag;

                        if(s_device_mcu_gpio.config.callbacks.callback_port2_pin7 != NULL)
                        {
                            if(transition & BIT7)
                            {
                                s_device_mcu_gpio.config.callbacks.callback_port2_pin7(HIGH_TO_LOW);
                            }
                            else
                            {
                                s_device_mcu_gpio.config.callbacks.callback_port2_pin7(LOW_TO_HIGH);
                            }
                        }
                    }
                    break;
                }
            }
        }
}

