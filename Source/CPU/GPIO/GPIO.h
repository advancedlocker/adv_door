/*
 * GPIO.h
 *
 *  Created on: Mar 19, 2018
 *      Author: Patrick
 */

#ifndef SOURCE_GPIO_H_
#define SOURCE_GPIO_H_

//INCLUDES
#include "Vlocker_Config.h"

//DEFINES
#define PORT1 1
#define PORT2 2
#define PORT3 3
#define PORT4 4

#define PIN0 0
#define PIN1 1
#define PIN2 2
#define PIN3 3
#define PIN4 4
#define PIN5 5
#define PIN6 6
#define PIN7 7

#ifdef __cplusplus
extern "C" {
#endif

typedef enum e_IO_DIRECTION
{
    INTERNAL,
    EXTERNAL,
}e_IO_DIRECTION_t;

typedef enum e_IO_LEVEL
{
    LOW,
    HIGH,
}e_IO_LEVEL_t;

typedef enum e_PnSEL
{
    PnSEL,
    PnSEL2,
}e_PnSEL_t;

typedef enum
{
    ENABLE,
    DISABLE,
}e_PnSEL_ENABLE_DISABLE_t;

typedef enum e_UP_DOWN
{
    UP,
    DOWN,
}e_UP_DOWN_t;

typedef enum
{
    LOW_TO_HIGH,
    HIGH_TO_LOW,
}transition_t;


//FORWARD DECLARATIONS

/**
 * @brief initilize all required ports and pins for this applicaiton
 */
void gpio_init(void);

/**
 * @brief set function type for specified port pin, i.e. timer(PnSEL=1),gpio(PnSEL=0),cap sensing(PnSEL2=1) etc.
 * @param port
 * @param pin
 * @param select
 * @param value
 * @param enable_disable enables or disables the pnSEL(1|2) control bit
 */
void gpio_set_function(uint8_t port, uint8_t pin, e_PnSEL_t select, e_PnSEL_ENABLE_DISABLE_t enable_disable);

/**
 *
 * @param port
 * @param pin
 * @param direction
 */
void gpio_set_direction(uint8_t port, uint8_t pin, e_IO_DIRECTION_t direction);

/**
 * @brief set internal pull up pull down resistor
 * @param port number
 * @param pin number
 * @param enable
 */
void gpio_set_pull_resistor(uint8_t port, uint8_t pin, bool enable);

/**
 * @brief get port pin number logic level value
 * @param port number
 * @param pin number
 * @return level (1|0) of pin
 */
e_IO_LEVEL_t gpio_get(uint8_t port, uint8_t pin);

/**
 *
 * @param port
 * @param pin
 * @return
 */
e_IO_LEVEL_t gpio_get_output(uint8_t port, uint8_t pin);

/**
 * @brief sets port pin number to logical level value
 * @param port number
 * @param pin number
 * @param level
 */
void gpio_set(uint8_t port, uint8_t pin, e_IO_LEVEL_t level);

/**
 * @brief de-initilize(s) all ports and pins
 */
void gpio_deinit(void);

/**
 * @brief sets the edge type interrupt
 * @param port to intertupt on
 * @param pin to interrupt on
 * @param transition 0 sets low-to-high transition interrupt, 1 sets high-to-low transition interrupt
 * @param callback to a function with the current transition type.
 * @param enable interrupt
 */
void gpio_set_edge_interrupt(uint8_t port, uint8_t pin,transition_t transition,void (*callback)(transition_t transition), bool enable);

#ifdef __cplusplus
}
#endif


#endif /* SOURCE_GPIO_H_ */
