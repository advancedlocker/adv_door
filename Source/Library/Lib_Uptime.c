/*
 * Lib_Uptime.c
 *
 *  Created on: Mar 19, 2018
 *      Author: Patrick
 */
#include <stdlib.h>
#include "Lib_Uptime.h"

/* Global uptime variable.  On most architectures this is probably
 * updated by timer interrupt service routine.
 */
volatile uptime_t uptime_ms_ = 0;

uint16_t LIB_UPTIME_get_module_version(void)
{
    return (LIB_UPTIME_MODULE_VERSION);
}

uptime_t get_uptime_ms(void)
{
    return uptime_ms_;
}

void uptime_timer_start(uptime_timer_t * timer, uint32_t delay_ms)
{
    uptime_t ctime = get_uptime_ms();
    timer->uptime = ctime + delay_ms;
    if (timer->uptime < ctime)
    {
        timer->wrap = 1;
    }
    else
    {
        timer->wrap = 0;
    }
    timer->is_started = 1;
}

unsigned long uptime_timer_is_expired(uptime_timer_t * timer)
{
    uptime_t ctime = get_uptime_ms();

    if (timer->wrap)
    {
        if (ctime > timer->uptime)
    {
            return 0;
    }
        /* uptime_ms_ has wrapped.
        */
        timer->wrap = 0;
        return 0;
    }
    return ctime >= timer->uptime;
}

void uptime_timer_stop(uptime_timer_t * timer)
{
    timer->is_started = 0;
}

int uptime_timer_is_started(uptime_timer_t * timer)
{
    return timer->is_started;
}

int32_t uptime_timer_remaining(uptime_timer_t * timer)
{
    int32_t remain = 0;

    if(timer->is_started)
    {
        remain = timer->uptime - get_uptime_ms();
    }
    return remain;
}

