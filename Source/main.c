#include <msp430.h>

#include <stdint.h>
#include "Vlocker_Config.h"
#include "Hardware.h"
#include "EEPROM.h"
#include "Lib_Uptime.h"
#include "UART_IO.h"

/**
 * main.c
 */
int main(void)
{
    WDTCTL = WDTPW + WDTHOLD;                 // Stop WDT

    P1DIR |= (1 << 2);
    P1DIR |= (1 << 3);
    P1DIR |= (1 << 7);

    P1OUT ^= (1 << 7);

    uint32_t serial = 0;

    uint8_t UART_CHAR = 0;

    EEPROM_Get_Serial(&serial);

    uptime_timer_t LED_Toggle_Timer = {0};

    hardware_init();

    while(1)
    {
        //while (!(UTCTL0 & TXEPT));              // Confirm no TXing before --> LPM3
        //TXBUF0 = UART_CHAR++;
        //_BIS_SR(GIE);
        if(uart_write_ready())
        {

        }
    }

    return 0;
}
