/*
 * Vlocker_Config.h
 *
 *  Created on: Mar 19, 2018
 *      Author: Patrick
 */

#ifndef SOURCE_VLOCKER_CONFIG_H_
#define SOURCE_VLOCKER_CONFIG_H_

//INCLUDES
#include <msp430.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <stddef.h>
#include <stdlib.h>
#include <stdbool.h>

#define VLOCK_SUCCESS     1
#define VLOCK_FAILURE     0


//DEFINES

/**@brief build states
*/
#define RELEASE_BUILD           0
#define DEBUG_BUILD             1

//HARDWARE DEFINES


//USCI
#define PORT_USCI               PORT3

#define PIN_USCI_SDA            PIN1
#define PIN_USCI_SCL            PIN2
#define PIN_USCI_TX             PIN4
#define PIN_USCI_RX             PIN5


#endif /* SOURCE_VLOCKER_CONFIG_H_ */
